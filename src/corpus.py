# -*- coding: UTF-8 -*-

from flask import Flask, request, render_template, session
from flask_babel import Babel, gettext

import settings

app = Flask(__name__)
babel = Babel(app)


@babel.localeselector
def get_locale():
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    return session.get('lang', 'be')


# Load default config and override config from an environment variable
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='sdfsdfsfdgdfg4fsd',
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


def run(form_options, lang):
    text = form_options['input'].split(' ')

    arab = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000]
    roman = ['I', 'IV', 'V', 'IX', 'X', 'XL', 'L', 'XC', 'C', 'CD', 'D', 'CM', 'M']

    # convert arab to roman
    if form_options['radio'] == 'radiobutton1':
        for i in range(len(text)):
            if text[i].isnumeric():
                temp = int(text[i])
                text[i] = ''
                j = len(arab) - 1
                while temp > 0:
                    if temp >= arab[j]:
                        text[i] += roman[j]
                        temp -= arab[j]
                    else:
                        j -= 1
    else:
        # convert roman to arab
        for i in range(len(text)):
            is_roman = False
            for j in range(len(text[i])):
                if text[i][j] in roman:
                    is_roman = True
                else:
                    is_roman = False
                    break
            if is_roman:
                temp = 0
                j = len(arab) - 1
                position = 0
                while j >= 0 & position < len(text[i]):
                    if text[i][position:position + len(roman[j])] == roman[j]:
                        temp += arab[j]
                        position += len(roman[j])
                    else:
                        j -= 1
                text[i] = str(temp)

    output_text = ''.join(str(text[i])+' ' for i in range(len(text)))

    return output_text


@app.route('/', methods=['GET', 'POST'])
def NumberConverter():
    lang = get_locale()
    form_options = {}
    _input = gettext('default input').replace('\\n', '\n')
    is_post = False
    output_text = ""
    if request.method == "POST":
        is_post = True
        _input = request.form.get("inputText")
        form_options['radio'] = request.form.get("mode")
        form_options['input'] = request.form.get("inputText")
        output_text = run(form_options, lang)
    return render_template('index.html', is_post=is_post, form_options=form_options, output_text=output_text,
                           _input=_input, lang=lang)


if __name__ == '__main__':
    app.run(use_reloader=True, debug=True, host=settings.host, port=settings.port)
